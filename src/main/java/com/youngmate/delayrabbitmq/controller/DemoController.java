package com.youngmate.delayrabbitmq.controller;

import com.youngmate.delayrabbitmq.component.DelaySender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: ZhaoYanqi
 */
@RestController
public class DemoController {

    @Autowired
    DelaySender delaySender;

    @GetMapping("send")
    public void sendMsg() {
        delaySender.send("延时消息测试");
    }
}
