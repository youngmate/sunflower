package com.youngmate.delayrabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 启动类
 *
 * @author Administrator
 */
@SpringBootApplication
public class DelayRabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(DelayRabbitMqApplication.class, args);
    }

}
