package com.youngmate.delayrabbitmq.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: ZhaoYanqi
 */
@Slf4j
@Component
@RabbitListener(queues = {"delayed-queue"})
public class DelayReceiver {
    @RabbitHandler
    public void process(String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("接收时间:{}", sdf.format(new Date()));
        log.info("消息内容:{}", msg);
    }
}
