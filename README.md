### 背景介绍
 生活中有这样一些业务场景：
  - 优惠券自领取之时起24小时内有效
  - 用户设置活动开始前N分钟提醒
  - 活动按照设定的时间结束
  ...

解决方案
1. 使用定时任务轮询订单表，查询用户券包中是否有应过期的优惠券，如果有查询出结果就执行过期处理。
> 缺点：假设每1分钟轮询一次，则会存在秒级误差，如果秒级轮询，则会极其消耗性能，影响程序的健壮性。
2. 用户领取优惠券时开启一个新线程，并将新线程按照设定的时间休眠，休眠结束后执行过期处理
> 缺点：高并发场景对服务器性能的影响；休眠期服务器更新、重启时线程将被销毁
3. 使用消息队列中的延时消息
> 缺点：部分MQ还不支持延时消息或有局限性

综合上述的几种解决方案，虽然部分MQ对延时消息的支持还不尽如人意，但有些消息中间件已经对此做了很好的支持，如RabbitMQ，如果原项目中使用的不是RabbitMQ，对比可用性和准确性，技术更新的成本更少，所有使用消息队列处理这类应用场景最为合适。

#### 架构
> SpringBoot + RabbitMQ 


> 本次演示是新建的项目，因为没有服务器所有都是在个人电脑上运行，如果有同样境遇的同学，则需要做以下一些准备工作
1. 安装Windows版RabbitMQ（需要erlang环境)
2. 配置好RabbitMQ用户权限
3. 安装rabbitMQ延时消息插件[rabbitmq_delayed_message_exchange](https://www.rabbitmq.com/community-plugins.html)并启动


#### 项目创建

```
IDEA可以便捷的帮助我们创建一个SpringBoot项目，由于过程过于简单，这里不做赘述。
```



#### 引入依赖

```xml
    <dependencies>
        <!--rabbit-mq-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-amqp</artifactId>
        </dependency>
        <!--lombok-->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
```


#### 配置application.properties

```yaml
#spring.application.name=springboot-rabbitmq
#
spring.rabbitmq.host=localhost
spring.rabbitmq.port=5672
spring.rabbitmq.username=test
spring.rabbitmq.password=123
spring.rabbitmq.listener.simple.acknowledge-mode=manual
```


#### RabbitMQ配置（队列、交换器、绑定关系）

```java
package com.youngmate.delayrabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: ZhaoYanqi
 */
@Configuration
public class RabbitMqConfig {

    public final static String QUEUE_NAME = "delayed-queue";
    public final static String EXCHANGE_NAME = "delay-exchange";


    /**
     * 配置队列
     */
    @Bean
    public Queue queue() {
        return new Queue(QUEUE_NAME);
    }

    /**
     * 配置交换器
     */
    @Bean
    public CustomExchange exchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        //参数二为类型：必须是x-delayed-message
        return new CustomExchange(EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }

    /**
     * 绑定队列到交换器
     */
    @Bean
    Binding binding(Queue queue, CustomExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME).noargs();
    }
}
```

#### 发送消息

```java
package com.youngmate.delayrabbitmq.component;

import com.youngmate.delayrabbitmq.config.RabbitMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 消息发送方
 *
 * @Author: ZhaoYanqi
 */
@Slf4j
@Component
public class DelaySender {
    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * 发送消息
     */
    public void send(String msg) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("发送时间:{}", sf.format(new Date()));
        rabbitTemplate.convertAndSend(RabbitMqConfig.EXCHANGE_NAME, RabbitMqConfig.QUEUE_NAME, msg, new MessagePostProcessor() {
            @Override
            public Message postProcessMessage(Message message) throws AmqpException {
                message.getMessageProperties().setHeader("x-delay", 10000);
                return message;
            }
        });
    }
}

```

#### 消费消息

```java
package com.youngmate.delayrabbitmq.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 消息监听和消费
 *
 * @Author: ZhaoYanqi
 */
@Slf4j
@Component
@RabbitListener(queues = {"delayed-queue"})
public class DelayReceiver {
    @RabbitHandler
    public void process(String msg) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("接收时间:{}", sdf.format(new Date()));
        log.info("消息内容:{}", msg);
    }
}

```

#### 调用演示

```java
package com.youngmate.delayrabbitmq.controller;

import com.youngmate.delayrabbitmq.component.DelaySender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 延时队列演示
 *
 * @Author: ZhaoYanqi
 */
@RestController
public class DemoController {

    @Autowired
    DelaySender delaySender;

    @GetMapping("send")
    public void sendMsg() {
        delaySender.send("延时消息测试");
    }
}

```


